#!/usr/bin/env python

import os, sys, time, logging
import operator,math
import glob, json, yaml

from django.core.management.base import BaseCommand, CommandError
from optparse import make_option

from django.contrib.sites.models import Site
from django.utils import timezone

#*******************************************************************************

from gestalt.web.helpers import Reactor

from gestalt.cmd.management import *

from uchikoma.connector.shortcuts import *
from uchikoma.console.shortcuts import *

from uchikoma.organizer.shortcuts import *
from uchikoma.entertain.shortcuts import *

from uchikoma.devops.shortcuts import *

from uchikoma.semantics.shortcuts import *
from uchikoma.opendata.shortcuts import *

################################################################################

class UnknownChannel(Exception):
    pass

#*******************************************************************************

class Command(Commandline):
    help = 'Initialize the database.'

    target_dir = property(lambda self: Reactor.SPECS('profile'))

    def add_arguments(self, parser):
        parser.add_argument("--clear", action="store_true",
            dest = "clear",
            help = "Clear platform before deploying.",
            #metavar = "FILE"
        )
        parser.add_argument("--sync", action="store_true",
            dest = "sync",
            help = "Synchronize data with DB.",
            #metavar = "FILE"
        )
        parser.add_argument("--static", action="store_true",
            dest = "static",
            help = "Collect static files.",
            #metavar = "FILE"
        )
        parser.add_argument("--specs", action="store_true",
            dest = "specs",
            help = "Import things from specs.",
            #metavar = "FILE"
        )
        parser.add_argument("--themes", action="store_true",
            dest = "themes",
            help = "Check existence of foreign themes.",
            #metavar = "FILE"
        )
        parser.add_argument("--nltk", action="store_true",
            dest = "nltk",
            help = "Download NLTK corpuses.",
            #metavar = "FILE"
        )

        for key,lst in self.MODE_MAPPING.iteritems():
            parser.add_argument("--"+key, action="store_true",
                dest = key,
                help = "Activate %s's mode flags : %s" % (key.capitalize(), ', '.join(lst)),
                #metavar = "FILE"
            )

    MODE_MAPPING = {
        'build':   ['themes','sync'],
        'release': ['clear', 'sync', 'specs','nltk','static'],
    }

    APP_MIGRATIONs = (
        'auth',
        'contenttypes','sites',
    ) + tuple(os.environ['GHOST_AGENTS'].split(' ')) + ('social_auth',)

    def handle(self, *args, **options):
        for key,lst in self.MODE_MAPPING.iteritems():
            if options[key]:
                for flg in lst:
                    options[flg] = True

        interpreter = 'django-admin'

        if os.path.exists('venv/bin/django-admin'):
            interpreter = 'venv/bin/django-admin'

        if options['sync']:
            for app in self.APP_MIGRATIONs:
                self.shell(interpreter, 'migrate', app)

            self.shell(interpreter, 'syncdb', '--noinput')

        if options['clear']:
            import django_rq

            qf = django_rq.get_failed_queue()

            print "*) Cleaning RQ : %d failed jobs ..." % qf.count

            qf.empty()

            from uchikoma.settings import RQ_QUEUES

            for key in [x for x in RQ_QUEUES.keys()]:
                qw = django_rq.get_queue(key)

                print "*) Cleaning RQ '%s' queue : %d jobs ..." % (qw.name,qw.count)

                qw.empty()

        if options['specs']:
            if 'GHOST_DOMAIN' in os.environ:
                for site in Site.objects.all():
                    if site.name in ('example.com',os.environ['GHOST_DOMAIN'],Reactor.domain):
                        site.name   = Reactor.domain
                        site.domain = Reactor.domain

                        site.save()

                    print "#] Updated site : %(name)s" % site.__dict__

        if options['static'] or options['themes']:
            for skin in Reactor.django.themes:
                path = Reactor.FILES('themes', skin)

                if not os.path.exists(path):
                    os.system('git clone git@bitbucket.org:kimino/%s.git %s --depth 1 --recursive' % (skin, path))

        if options['static']:
            self.shell(interpreter, 'collectstatic', '-l', '--noinput')

        if options['specs']:
            print "#] Ensuring users :"

            for alias in os.listdir(self.rpath()):
                if alias.endswith('.yml'):
                    alias = alias.replace('.yml','')

                    specs = yaml.load(self.read('%s.yml' % alias))

                    user,st = Identity.objects.get_or_create(
                        username = alias,
                        email    = specs['email'],
                    )

                    for field in ('first_name','last_name'):
                        if field in specs:
                            setattr(user, field, specs[field])

                    if type(specs.get('roles',[])) not in (tuple,set,frozenset,list):
                        specs['roles'] = specs.get('roles', [])

                    for role in specs['roles']:
                        if hasattr(user, 'is_'+role):
                            setattr(user, 'is_'+role, True)

                    if 'password' in specs:
                        user.set_password(specs['password'])

                    user.save()

                    print ("\t*) "+('Added' if st else 'Updated')+" user '%(username)s' with address %(email)s :") % user.__dict__

                    for mail in specs.get('mailbox', []):
                        box,st = MailBox.objects.get_or_create(
                            owner    = user,
                            username = mail['username'],
                        )

                        for key in mail:
                            if key not in ['username']:
                                setattr(box, key, mail[key])

                        box.save()

                        print ("\t\t-> "+('Added' if st else 'Updated')+" mailbox '%(username)s' at %(hostname)s/%(port)d.") % box.__dict__

                    for feed in specs.get('cables', []):
                        rss,st = RssFeed.objects.get_or_create(
                            owner = user,
                            link  = feed['link'],
                        )

                        for key in feed:
                            if key not in ['link']:
                                setattr(rss, key, feed[key])

                        rss.when     = timezone.now()

                        rss.save()

                        print ("\t\t-> "+('Added' if st else 'Updated')+" feed at : %(link)s") % rss.__dict__

                    #user.refresh()

        if options['nltk']:
            import nltk

            corporas = os.environ.get('NLTK_CORPUS', 'book').split(' ')

            for corp in corporas:
                nltk.download(corp, download_dir=os.environ['NLTK_DATA'])

        if options['nltk']:
            pass

