#!/usr/bin/env python

import os, sys, time, logging
import operator,math
import glob, json, yaml

from django.core.management.base import BaseCommand, CommandError

from gestalt.web.helpers import Reactor

from django.contrib.sites.models import Site
from uchikoma.connector.models import Identity

import django_rq

################################################################################

class UnknownChannel(Exception):
    pass

#*******************************************************************************

class Command(BaseCommand):
    help = 'Cleanup the platform.'

    def add_arguments(self, parser):
        parser.add_argument("--rq-queues", action="store_true",
            dest = "rq-queues",
            help = "Clear all Redis-Queue jobs.", 
            #metavar = "FILE"
        )
        parser.add_argument("--rq-failed", action="store_true",
            dest = "rq-failed",
            help = "Clear failed Redis-Queue jobs.", 
            #metavar = "FILE"
        )

    target_dir = property(lambda self: Reactor.FILES('agents'))
    def rpath(self, *x): return os.path.join(self.target_dir, *x)

    def shell(self, *cmd):
        wrap_cmd = lambda txt: '"'+txt+'"' if (' ' in txt) else txt

        stmt = ' '.join([ wrap_cmd(txt) for txt in cmd ])

        os.system(stmt)

    def handle(self, *args, **options):
        if options['rq-failed']:
            qf = django_rq.get_failed_queue()

            print "*) Cleaning RQ : %d failed jobs ..." % qf.count

            qf.empty()

        if options['rq-queues']:
            target = [x for x in settings.RQ_QUEUES.keys()]

            for key in target:
                qw = django_rq.get_queue(key)

                print "*) Cleaning RQ '%s' queue : %d jobs ..." % (qw.name,qw.count)

                qw.empty()

