from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

import facebook

################################################################################

@Reactor.social.register_adapter('facebook')
class FacebookAPI(Reactor.social.API):
    VERSION = '2.5'

    OGP_FIELDs = (
        'id','is_verified',
        'name','link','location',
        'about','bio','cover',
        'birthday','context',
    )

    def initialize(self):
        pass # GooglePlus_People(self, 'people')

        self._grp = facebook.GraphAPI(access_token=self.api_token)

    graph = property(lambda self: self._grp)

    def retrieve(self, mapper, ancestor, path, **lookup):
        resp = self.graph.get_object(path)

        if 'data' in resp:
            for entry in resp['data']:
                item = mapper(self, ancestor, entry)

                yield item
        else:
            pass # return resp['items']

    def populate(self, mapper, ancestor, path, **lookup):
        #resp = self.get_object(path, **lookup)

        resp = self.graph.get_objects(path)

        if 'data' in resp:
            for entry in resp['data']:
                item = mapper(self, ancestor, entry)

                yield item
        else:
            pass # return resp['items']

    class Wrapper(Reactor.social.Wrapper):
        pass

#*******************************************************************************

@Reactor.social.register_wrapper('facebook','user')
class FacebookUser(Reactor.social.Resource):
    OGP_FIELDs = FacebookAPI.OGP_FIELDs+(
        'email',
        'first_name','middle_name','last_name',
        'gender','interested_in','meeting_for','relationship_status',
        'locale','hometown',
        'education','quotes','inspirational_people',
        'political','religion','test_group',
        'currency','payment_pricepoints',
        'sports','favorite_athletes','favorite_teams',
    )

    class Wrapper(FacebookAPI.Wrapper):
        pass

#*******************************************************************************

@Reactor.social.register_wrapper('facebook','page')
class FacebookPage(Reactor.social.Resource):
    OGP_FIELDs = FacebookAPI.OGP_FIELDs+(
        #'age',
    )

    class Wrapper(FacebookAPI.Wrapper):
        pass

################################################################################

@Reactor.social.register_wrapper('facebook','event')
class FacebookEvent(Reactor.social.Resource):
    OGP_FIELDs = (
        'name','cover','category',
        'place','start_time','end_time',
    )

    class Wrapper(FacebookAPI.Wrapper):
        pass

#*******************************************************************************

class FacebookPost(Reactor.social.Resource):
    OGP_FIELDs = (
        'application','actions','created_time','child_attachments','caption','event','description','from','full_picture','icon','height','id','coordinates','instagram_eligibility','comments_mirroring_domain','link','message','object_id','parent_id','permalink_url','name','picture','place','privacy','properties','shares','source','status_type','story','story_tags','subscribed','type','updated_time','via','width','attachments','sharedposts','to','message_tags',
        'likes','comments', # 'reactions',
    )

    class BaseWrapper(FacebookAPI.Wrapper):
        pass

################################################################################

@Reactor.social.register_wrapper('facebook','status')
class FacebookStatus(FacebookPost):
    class Wrapper(FacebookPost.BaseWrapper):
        pass

#*******************************************************************************

@Reactor.social.register_wrapper('facebook','link')
class FacebookLink(FacebookPost):
    class Wrapper(FacebookPost.BaseWrapper):
        pass

#*******************************************************************************

@Reactor.social.register_wrapper('facebook','image')
class FacebookImage(FacebookPost):
    class Wrapper(FacebookPost.BaseWrapper):
        pass

#*******************************************************************************

@Reactor.social.register_wrapper('facebook','video')
class FacebookVideo(FacebookPost):
    def populate(self):
        resp = self.query()

    class Wrapper(FacebookPost.BaseWrapper):
        pass

