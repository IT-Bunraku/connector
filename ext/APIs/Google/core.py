from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

################################################################################

@Reactor.social.register_adapter('google')
class GoogleAPI(Reactor.social.API):
    def initialize(self):
        GooglePlus_People(self, 'people')

    def query(self, svc, path, **lookup):
        lookup['access_token'] = self.api_token

        resp = requests.get('https://www.googleapis.com/%s/%s' % (svc,path),
            params=lookup,
        )

        resp = req.json()

        return resp

    def retrieve(self, mapper, ancestor, svc, path, **lookup):
        resp = self.query(svc, path, **lookup)

        if 'items' in resp:
            for entry in resp['items']:
                item = mapper(api, ancestor, entry)

                yield item
        else:
            pass # return resp['items']

    #***************************************************************************

    class Service(Reactor.social.Service):
        service_name = property(lambda self: '%s/%s' % (self.SERVICE, self.VERSION))

        def rpath(self, *parts):
            return self.PATH_SEP.join([self.MODULE]+[x for x in parts])

        def query(self, *path, **kwargs):
            return self.api.query(self.service_name, self.rpath(*path), **kwargs)

        def retrieve(self, mapper, ancestor, *path, **kwargs):
            return self.api.retrieve(mapper, ancestor, self.service_name, self.path, **kwargs)

