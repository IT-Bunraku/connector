from uchikoma.connector.ext.APIs.Google.core import *

################################################################################

@Reactor.social.register_service('google','people')
class GooglePlus_People(GoogleAPI.Service):
    SERVICE = 'plus'
    VERSION = 'v1'
    MODULE  = 'people'

    PATH_SEP = '/'

    def discover(self):
        for contact in self.retrieve(vContact, {}, 'me', 'connections'):
            yield contact

#*******************************************************************************

@Reactor.social.register_wrapper('google','contact')
class vContact(Reactor.social.Resource):
    pass

