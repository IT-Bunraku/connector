from uchikoma.connector.ext.APIs.Google.core import *

################################################################################

@Reactor.social.register_service('google','calendar')
class GoogleCalendar(GoogleAPI.Service):
    SERVICE = 'calendar'
    VERSION = 'v1'
    MODULE  = 'calendar'

    PATH_SEP = '/'

    def discover(self):
        for contact in self.retrieve(vCalendar, {}, 'me', 'event'):
            yield contact

#*******************************************************************************

@Reactor.social.register_wrapper('google','event')
class vCalendar(Reactor.social.Resource):
    pass

