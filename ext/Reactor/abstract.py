#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

###################################################################################

class BaseAPI(object):
    def __init__(self, user_id, access_token, **config):
        from uchikoma.connector.models import Identity

        self._cfg = config
        self._key = access_token

        self._usr = Identity.objects.get(pk=user_id)

    config = property(lambda self: self._cfg)
    token  = property(lambda self: self._key)
    ident  = property(lambda self: self._usr)

#*******************************************************************************

class SubAPI(object):
    def __init__(self, parent, alias=None):
        if alias is None:
            if hasattr(self, 'MODULE'):
                 alias = self.MODULE
            else:
                 alias = self.__name__

        self._api = parent
        self._key = alias

        if not hasattr(self.parent, self.alias):
            setattr(self.parent, self.alias, self)

    parent = property(lambda self: self._api)
    alias  = property(lambda self: self._key)

    api    = property(lambda self: self.parent)

#*******************************************************************************

class BaseResource(object):
    def __init__(self, api, alias):
        self._prn = api
        self._key = alias

    parent   = property(lambda self: self._prn)
    alias    = property(lambda self: self._key)

    api      = property(lambda self: self._api)

#*******************************************************************************

class BaseWrapper(object):
    def __init__(self, parent, resource, narrow, raw_item):
        self._prn = parent
        self._res = resource
        self._nrw = narrow
        self._raw = raw_item

    parent   = property(lambda self: self._prn)
    resource = property(lambda self: self._res)
    narrow   = property(lambda self: self._nrw)

    raw_item = property(lambda self: self._raw)

