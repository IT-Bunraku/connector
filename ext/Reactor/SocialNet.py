#-*- coding: utf-8 -*-

from gestalt.core.helpers import *

from .abstract import *

###################################################################################

@extend_reactor('social')
class Platform_Social(ReactorExt):
    def initialize(self):
        self._adapt = {}
        self._svcs  = {}
        self._wraps = {}

    adapters   = property(lambda self: self._adapt)
    services   = property(lambda self: self._svcs)
    wrappers   = property(lambda self: self._wraps)

    #***************************************************************************

    def api_manager(self, user):
        for app,ns in self.reactor.django.apps:
            __import__('%s.ext.APIs' % ns)

        from uchikoma.connector.models import Identity

        if type(user) in (int,long,str,unicode):
            user = Identity.objects.get(pk=user)

        resp = {}

        for backend in user.social_auth.all():
            key = backend.provider

            for part in ('-oauth', '-oauth2'):
                if part in key:
                    key = key.replace(part, '')

            if key in self._adapt:
                handler = self._adapt[key]

                cnt = backend.extra_data

                if type(cnt) in (str,unicode):
                    cnt = json.loads(cnt)

                api = handler(user.pk, **cnt)

                resp[key] = api

                #for entry in api.discover():
                #    resp[] = entry
            else:
                print "Could'nt find provider for : %s (%s)" % (key, backend.provider)

        return resp

    ############################################################################

    def register_adapter(self, *args, **kwargs):
        def do_apply(handler, alias=None):
            if alias not in self._adapt:
                self._adapt[alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_service(self, *args, **kwargs):
        def do_apply(handler, adapter, service, alias=None):
            if adapter not in self._svcs:
                self._svcs[adapter] = {}

            if alias not in self._svcs[adapter]:
                self._svcs[adapter][alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #***************************************************************************

    def register_wrapper(self, *args, **kwargs):
        def do_apply(handler, adapter, alias=None):
            if adapter not in self._wraps:
                self._wraps[adapter] = {}

            if alias not in self._wraps[adapter]:
                self._wraps[adapter][alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    ############################################################################

    class API(BaseAPI):
        pass

    #***************************************************************************

    class Service(SubAPI):
        pass

    #***************************************************************************

    class Resource(BaseResource):
        pass

    #***************************************************************************

    class Wrapper(BaseWrapper):
        pass

