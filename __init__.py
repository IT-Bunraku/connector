subdomains = {
    'apis':     dict(icon='key',         title="Authentify"),
    'contacts': dict(icon='credit-card', title="Contacts"),
    'agenda':   dict(icon='calendar',    title="Agenda"),
}

