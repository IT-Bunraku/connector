from .utils import *

from django.contrib.auth.models import AbstractUser

from subdomains.utils import reverse

################################################################################

from django.forms import Form
from django_enumfield import enum

class Gender(enum.Enum):
    MALE = 1
    FEMALE = 2

    labels = {
        MALE: 'Male',
        FEMALE: 'Female',
    }

@Reactor.orm.register_model('identity')
class Identity(AbstractUser):
    gender  = enum.EnumField(Gender)

    jobs    = models.CharField(max_length=256, blank=True)
    about   = models.TextField(blank=True)

    city    = models.CharField(max_length=64, blank=True)
    country = CountryField(blank=True)

    phone   = PhoneNumberField(blank=True)

    @property
    def context(self):
        return {
            
        }

    full_name = property(lambda self: '%(last_name)s %(first_name)s' % self.__json__)
    mugshot    = property(lambda self: '/media/mugshot/%s.jpg' % self.username)

    def get_absolute_url(self): return reverse('public_profile', 'connect', owner=self.username)

    #***************************************************************************

    @property
    def contacts(self):
        from .schemas import Contact

        return Contact.objects()

        from .schemas import populate_schemas, populate_func_contact, populate_sort_contact
        from .schemas import CONTACT_TYPEs

        return populate_schemas(populate_func_contact, populate_sort_contact, *CONTACT_TYPEs,
            owner_id=self.pk
        )

    @property
    def mailing(self):
        from .schemas import populate_schemas, populate_func_document, populate_sort_document
        from .schemas import MAIL_TYPEs

        return populate_schemas(populate_func_document, populate_sort_document, *MAIL_TYPEs,
            owner_id=self.pk
        )

    #***************************************************************************

    @property
    def calendar(self):
        from .schemas import populate_schemas, populate_func_document, populate_sort_document
        from .schemas import CALENDAR_TYPEs

        return populate_schemas(populate_func_document, populate_sort_document, *CALENDAR_TYPEs,
            owner_id=self.pk
        )

    @property
    def timeline(self):
        from .schemas import populate_schemas, populate_func_document, populate_sort_document
        from .schemas import TIMELINE_TYPEs

        return populate_schemas(populate_func_document, populate_sort_document, *TIMELINE_TYPEs,
            owner_id=self.pk
        )

    #***************************************************************************

    @property
    def posts(self):
        from .schemas import populate_schemas, populate_func_document, populate_sort_document
        from .schemas import POST_TYPEs

        return populate_schemas(populate_func_document, populate_sort_document, *POST_TYPEs,
            owner_id=self.pk
        )

    @property
    def activity(self):
        from .schemas import populate_schemas, populate_func_document, populate_sort_document
        from .schemas import ACTIVITY_TYPEs

        return populate_schemas(populate_func_document, populate_sort_document, *ACTIVITY_TYPEs,
            owner_id=self.pk
        )

    @property
    def __json__(self):
        resp = self.__dict__

        resp.update(self.context)

        return resp

################################################################################

@Reactor.orm.register_model('organization')
class Organization(models.Model):
    name         = models.CharField(max_length=64)
    title        = models.CharField(max_length=256, blank=True)

    created_on   = models.DateTimeField(auto_now_add=True)

    #heroku_regex = models.CharField(max_length=256, blank=True)
    #github_regex = models.CharField(max_length=256, blank=True)
    #bucket_regex = models.CharField(max_length=256, blank=True)

    def __str__(self):     return str(self.title or self.name)
    def __unicode__(self): return unicode(self.title or self.name)

################################################################################

API_IMPLANTs = (
    ('endpoint', "Endpoint API"),
    ('social',   "Social API"),
    ('token',    "Token API"),
)

@Reactor.orm.register_model('api_provider')
class ApiProvider(models.Model):
    alias   = models.CharField(max_length=64)
    title   = models.CharField(max_length=128, blank=True)

    implant = models.CharField(max_length=16, choices=API_IMPLANTs, default='token')

    def __str__(self):     return str(self.title or self.alias)
    def __unicode__(self): return unicode(self.title or self.alias)

    @property
    def handler(self):
        resp = None

        return resp

    class Meta:
            verbose_name        = "API provider"
            verbose_name_plural = "API providers"

#*******************************************************************************

@Reactor.orm.register_model('api_endpoint')
class ApiEndpoint(models.Model):
    owner    = models.ForeignKey(Identity, related_name='api_endpoints')
    alias    = models.CharField(max_length=48)

    link     = models.CharField(max_length=256)
    provider = models.ForeignKey(ApiProvider, related_name="endpoints", blank=True, null=True, default=None)

    title    = models.CharField(max_length=128, blank=True)
    config   = JSONField(default={})

    @property
    def proxy(self):
        resp = None

        return resp

    #def save(self, *args, **kwargs):
    #    qs = FS_Provider.objects.filter(alias=urlparse(self.link).scheme)

    #    if len(qs):
    #            self.provider = qs[0]

    #    return super(ApiEndpoint, self).save(*args, **kwargs)

    def __str__(self):     return str(self.title or self.alias)
    def __unicode__(self): return unicode(self.title or self.alias)

    class Meta:
            verbose_name        = "API endpoint"
            verbose_name_plural = "API endpoints"

#*******************************************************************************

class WebResource(models.Model):
    @property
    def __json__(self):
        resp = self.__dict__

        resp.update(self.context)

        return resp

    def __str__(self):     return str(self.title or self.alias)
    def __unicode__(self): return unicode(self.title or self.alias)

    class Meta:
        abstract = True

