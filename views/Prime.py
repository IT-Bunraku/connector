from uchikoma.connector.shortcuts import *

################################################################################

# @Reactor.router.register_route('connect', r'^organizations$', strategy='login')
@login_required
@render_to('rest/organization/list.html')
def org__list(request):
    from uchikoma.console.models import Organization

    return context(
        listing=[
            org.__json__
            for org in Organization.objects.all()
        ],
    )

#*******************************************************************************

# @Reactor.router.register_route('connect', r'^organizations$', strategy='login')
@login_required
@render_to('rest/organization/view.html')
def org__view(request, nrw='self'):
    from uchikoma.console.models import Organization

    org = Organization.objects.get(alias=nrw)

    return context(org.__json__)

################################################################################

@login_required
@render_to('rest/identity/list.html')
def identity__list(request):
    return context(
        listing=[
            idn
            for idn in Identity.objects.all()
        ],
    )

#*******************************************************************************

@login_required
@render_to('rest/identity/view.html')
def identity__view(request, nrw=None):
    idn = request.user

    if nrw not in (None, 'me'):
        idn = Identity.objects.get(username=nrw)

    return context(
        profile=idn,
    )

################################################################################

@login_required
@render_to('rest/network/list.html')
def network__list(request):
    #idn = Identity.objects.get(alias=owner)

    idn = request.user

    if request.method=='POST':
        idn.refresh()

    return context(
        listing=[], # Reactor.social.api_manager(idn).values(),
    )

#*******************************************************************************

@login_required
@render_to('rest/network/view.html')
def network__view(request, adapter):
    idn = request.user

    lst = Reactor.social.api_manager(idn)

    if adapter in lst:
        return context(
            profile=idn,
            network=lst[adapter],
        )
    else:
        raise Http404("Network '%s' not found !" % adapter)

#*******************************************************************************

@login_required
@render_to('rest/network/graph.html')
def network__graph(request, narrow):
    #idn = Identity.objects.get(alias=owner)

    idn = request.user

    repo = idn.repos.get(alias=nrw)

    return context(repo.__json__)

#*******************************************************************************

@login_required
@render_to('rest/network/actions.html')
def network__actes(request, narrow):
    #idn = Identity.objects.get(alias=owner)

    idn = request.user

    repo = idn.repos.get(alias=nrw)

    return context(repo.__json__)

#*******************************************************************************

@login_required
@render_to('rest/network/events.html')
def network__events(request, narrow):
    #idn = Identity.objects.get(alias=owner)

    idn = request.user

    repo = idn.repos.get(alias=nrw)

    return context(repo.__json__)

#*******************************************************************************

@login_required
@render_to('rest/network/hooks.html')
def network__hooks(request, narrow, name):
    #idn = Identity.objects.get(alias=owner)

    idn = request.user

    repo = idn.repos.get(alias=nrw)

    return context(repo.__json__)

