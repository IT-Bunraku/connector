#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

from django.contrib.auth import authenticate as dj_auth_check
from django.contrib.auth import login        as dj_auth_login

################################################################################

def login(request):
    frm = LoginForm()

    if request.method=='POST':
        frm = LoginForm(request.POST)

        if frm.is_valid():
            idn = dj_auth_check(username=frm.cleaned_data['pseudo'], password=frm.cleaned_data['passwd'])

            if idn is not None:
                dj_auth_login(request, idn)

    if request.user.is_authenticated():
       target = request.GET.get('next')

       if target:
           return HttpResponseRedirect(target)
       else:
           return redirect('homepage')

    return request.render('apis/anonymous.html',
        form = frm,
    )

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return redirect('/')

#*******************************************************************************

@render_to('apis/anonymous.html')
def validation_sent(request):
    return context(
        validation_sent=True,
        email=request.session.get('email_validation_address')
    )

@render_to('apis/anonymous.html')
def require_email(request):
    backend = request.session['partial_pipeline']['backend']
    return context(email_required=True, backend=backend)

@psa('social:complete')
def ajax_auth(request, backend):
    if isinstance(request.backend, BaseOAuth1):
        token = {
            'oauth_token': request.REQUEST.get('access_token'),
            'oauth_token_secret': request.REQUEST.get('access_token_secret'),
        }
    elif isinstance(request.backend, BaseOAuth2):
        token = request.REQUEST.get('access_token')
    else:
        raise HttpResponseBadRequest('Wrong backend type')
    user = request.backend.do_auth(token, ajax=True)
    login(request, user)
    data = {'id': user.id, 'username': user.username}
    return HttpResponse(json.dumps(data), mimetype='application/json')

