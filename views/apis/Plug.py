#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

################################################################################

# @Reactor.router.register_route('hub', r'^plug$', strategy='login')
@render_to('apis/views/plug/landing.html')
def home(request):
    #import code2use.backends.APIs
    #import code2use.backends.Endpoints

    #from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use, Social2use

    cnt = {
        'apis':      Reactor.social.api_manager(request.user).iteritems(),
        'endpoints': {},
    }

    #cnt['networks'] = cnt['apis'].keys()

    for alias,handler in [
        ('rss',     'rss_feeds'),
        ('mail',    'mailboxes'),
        ('wp',      'wordpress'),

        ('dyno',    'ops_dynos'),
        ('backend', 'ops_backends'),
    ]:
        manager = getattr(request.user, handler, None)

        if manager is not None:
            cnt['endpoints'][alias] = manager.all()

    return cnt

################################################################################

# @Reactor.router.register_route('hub', r'^plug/api/(?P<provider>.+)$', strategy='login')
@render_to('apis/views/plug/api.html')
def do_api(request, provider):
    #from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use

    #import code2use.backends.APIs

    return {
        'provider': provider,
    }

#*******************************************************************************

# @Reactor.router.register_route('hub', r'^plug/endpoint/(?P<provider>.+)$', strategy='login')
@render_to('apis/views/plug/endpoint.html')
def do_endpoint(request, provider):
    #from code2use.core.abstract import Ontology2use, Cloud2use, Endpoint2use

    #import code2use.backends.Endpoints

    return {
        'provider': provider,
    }

