#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

################################################################################

@Reactor.router.register('apis', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "apis/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

#*******************************************************************************

@Reactor.router.register('apis', r'^refresh$', strategy='login')
def refresher(request):
    messages.add_message(request, messages.INFO, "*) Started refreshing for '%s' ..." % request.user.username)

    networks = []

    Reactor.rq.invoke_task('refresh_identity', request.user.pk, *networks,
        wordpress=True,
    mail=True, feed=True)

    return redirect('/')

#*******************************************************************************

@Reactor.router.register('apis', r'^settings$', strategy='login')
@render_to('apis/settings.html')
def settings(request):
    frm = SettingsForm(instance=request.user)

    if request.method=='POST':
        frm = SettingsForm(request.POST, instance=request.user)

        if frm.is_valid():
            idn = frm.save(commit=False)

            idn.save()

    return context(
        identity=request.user,
        form=frm,
    )

################################################################################

@Reactor.router.register('apis', r'^v2/identities/', strategy='login')
class IdentityResource(TastyModel):
    class Meta:
        queryset = Identity.objects.all()
        resource_name = 'identity'
        detail_uri_name = 'username'

    def prepend_urls(self):
        return [
            dj_urls.url(r"^@(?P<username>[\w\d_.-]+)/?$", self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

#***************************************************************

@Reactor.router.register('apis', r'^v2/organizations/$', strategy='login')
class OrganizationResource(TastyModel):
    class Meta:
        queryset = Organization.objects.all()
        resource_name = 'organization'
        detail_uri_name = 'name'

    def prepend_urls(self):
        return [
            dj_urls.url(r"^~(?P<name>[\w\d_.-]+)/$", self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

#***************************************************************

@Reactor.router.register('apis', r'^v2/networks/$', strategy='login')
class ProviderResource(TastyModel):
    class Meta:
        queryset = ApiProvider.objects.all()
        resource_name = 'network'
        detail_uri_name = 'alias'

    def prepend_urls(self):
        return [
            dj_urls.url(r"^-(?P<alias>[\w\d_.-]+)/$", self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

################################################################################

#from . import Endpoints

from . import Auth
from . import Plug

