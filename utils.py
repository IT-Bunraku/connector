from gestalt.web.utils       import *

import socket, requests, simplejson as json

#*******************************************************************************

VORTEX_DEVOPS = 'http://scuba_dev:zdZ4ejSxxgMMtRdSwhcT@scubadev.sb05.stations.graphenedb.com:24789'

def view_cypher(target, lookup, labels=[], mapping={}):
    link = urlparse(target)

    req = requests.post('http://%s:%d/db/data/transaction/commit' % (link.hostname,link.port), data=json.dumps({
        "statements":[{
            "statement": lookup,
            "resultDataContents": ["row","graph"],
        }],
    }), auth=requests.auth.HTTPBasicAuth(link.username, link.password))

    resp = req.json()

    nodes, edges = {}, []

    vice = {}

    for result in resp['results']:
        for entry in result['data']:
            for n in entry['graph']['nodes']:
                extra = mapping.get(n['properties']['ontology'], None)

                extra = extra or (lambda n,p: p.values()[0])

                nodes[n['id']] = {
                    "label":  extra(n, n['properties']),
                    "fill":   "rgba(0,127,255,0.06)",
                    "stroke": "rgba(0,0,0,0.80)",
                }

            for e in entry['graph']['relationships']:
                edges.append([e['startNode'], e['endNode'], {"stroke":"rgba(0,0,0,0.60)"}])

    return nodes, edges

################################################################################

DYNO_STATEs = (
    ('sleepy', "Sleepy & Inactive"),
    ('active', "Active & Alive"),
    ('stale',  "Inexistant & Deleted"),
)

DYNO_REPOs = (
    ('github',    "GitHub Inc"),
    ('bitbucket', "Atlassian BitBucket"),
    ('gitlab',    "GitLab Community"),
)

DYNO_PLATFORMs = (
    ('heroku',    "Heroku Dyno"),
    ('openshift', "OpenShift App"),
)

DYNO_BACKENDs = (
    ('mlab',       "mLab MongoDB"),
    ('graphenedb', "Graphene DB"),
    ('logentries', "LogEntries"),
    ('newrelic',   "NewRelic Insights"),
)

#*******************************************************************************

def downsert(ontology, entry, user_id, network):
    handler = getattr(ontology, 'from_%s' % network, None)

    nrw,dtn = None,None

    if callable(handler):
        #try:
        nrw,dtn = handler(user_id, entry)
        #except Exception,ex:
        #    raise ex

        if nrw is not None:
            upsert(ontology, nrw, dtn)

#*******************************************************************************

def upsert(coll, nrw, data={}, cb=None):
    doc = None

    qs = coll.objects(**nrw)

    if len(qs):
        doc = qs[0]
    else:
        doc = coll(**nrw)

    if doc:
        for prop in data:
            setattr(doc, prop, data[prop])

        try:
            doc.save()
        except Exception,ex:
            print ex

    if callable(cb):
        try:
            cb(coll, doc)
        except Exception,ex:
            print ex

    if hasattr(coll, 'ns_alias'):
        from django.core.cache import caches

        vault = caches['default']

        vault.delete('enumerate_schema_%s' % coll.ns_alias)

    #print "\t-> (%s) %s => %s" % (doc.id, nrw, data)

    return doc

