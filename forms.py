from .utils import *

from .models import *

class LoginForm(forms.ModelForm):
    class Meta:
        model   = Identity
        fields  = ('username','password')

class SettingsForm(forms.ModelForm):
    class Meta:
        model   = Identity
        exclude = (
            'username','password',
            'is_superuser','is_staff','is_active',
            'groups','user_permissions',
            'last_login','date_joined',
        )

