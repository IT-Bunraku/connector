from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin

from tastypie.admin import ApiKeyInline

from .models import *
#from .graph import *

#from neo4django import admin as neo_admin

################################################################################

#class PersonAdmin(neo_admin.ModelAdmin):
#    pass

#neo_admin.site.register(Person, PersonAdmin)

################################################################################

class IdentityAdmin(ImportExportActionModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'is_staff', 'is_superuser')
    inlines = UserAdmin.inlines + [ApiKeyInline]

admin.site.register(Identity, IdentityAdmin)

#*******************************************************************************

class OrganizationAdmin(ImportExportActionModelAdmin):
    list_display = ('name', 'title', 'created_on')
    list_filter  = ('created_on',)

admin.site.register(Organization, OrganizationAdmin)

################################################################################

class ProviderAdmin(ImportExportActionModelAdmin):
    list_display = ('alias', 'title', 'implant')
    list_filter  = ('implant',)

admin.site.register(ApiProvider, ProviderAdmin)

#*******************************************************************************

class EndpointAdmin(ImportExportActionModelAdmin):
    list_display = ('owner', 'alias', 'title', 'link', 'provider')
    list_filter  = ('provider__alias', 'owner__username')

admin.site.register(ApiEndpoint, EndpointAdmin)

