from django.conf.urls import include, url

from django.conf.urls.static import static

from django.conf import settings

from gestalt.web.helpers import Reactor

from uchikoma.connector.views      import Prime, apis as Views
#from uchikoma.connector.views.apis import Auth, Endpoints, Plug
from uchikoma.connector.views.apis import Auth, Plug

################################################################################

urlpatterns = Reactor.router.urlpatterns('apis',
    url(r'^endpoints/', Reactor.router.apipatterns('v1')),

    url(r'^oauth2/',    include('provider.oauth2.urls',           namespace='oauth2')),
    url(r'^social/',    include('social.apps.django_app.urls',    namespace='social')),

    url(r'^ajax/(?P<backend>[^/]+)/$',                  Auth.ajax_auth,          name='ajax-auth'),
    url(r'^email/$',                                    Auth.require_email,      name='require_email'),
    url(r'^email/sent/?',                               Auth.validation_sent),
    url(r'^login/?$',                                   Auth.login,              name='login'),
    url(r'^logout/?$',                                  Auth.logout,             name='logout'),

    url(r'^profile$',                                   Prime.identity__view,     name='profile'),
    url(r'^networks$',                                  Prime.network__list,      name='networks'),
    url(r'^networks/$',                                 Prime.network__list,      name='networks'),

    url(r'^-(?P<adapter>[^/]+)/$',                      include([
        url(r'^$',                      Prime.network__view,      name='network__view'),
        url(r'^ontologies$',            Prime.network__graph,     name='network__graph'),
        url(r'^actions$',               Prime.network__actes,     name='network__actions'),
        url(r'^events$',                Prime.network__actes,     name='network__events'),
        url(r'^hooks/(?P<name>[^/]+)$', Prime.network__hooks,     name='network__hooks'),
    ])),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

