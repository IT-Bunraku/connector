from .utils import *

from .models import *

################################################################################

@Reactor.mongo.register_schema('contact')
class Contact(MongoDocument):
    ns_alias = 'contact'
    ns_icon  = 'user'
    ns_types = []

    address  = MongoProp.StringField(max_length=255, required=True, primary_key=True)
    fullname = MongoProp.StringField(max_length=255, required=True)

    intro    = MongoProp.StringField(required=False, default='')
    bio      = MongoProp.StringField(required=False, default='')

    mugshot  = MongoProp.StringField(required=False, default='')

    #location = MongoProp.DictField(required=False)
    phone    = MongoProp.StringField(required=False, default='')

#*******************************************************************************

@Reactor.mongo.register_embed('location')
class Location(MongoEmbedded):
    latitude  = MongoProp.IntField()
    longitude = MongoProp.IntField()

    address   = MongoProp.StringField(verbose_name="Full Address")
    postalbox = MongoProp.StringField(verbose_name="Postal Code")
    city      = MongoProp.StringField(verbose_name="City")
    country   = MongoProp.StringField(verbose_name="Country")

#*******************************************************************************

@Reactor.mongo.register_embed('tag')
class CustomTag(MongoEmbedded):
    #author    = MongoProp.ReferenceField(Contact)
    content   = MongoProp.StringField(verbose_name="Comment", required=True)

#*******************************************************************************

@Reactor.mongo.register_embed('comment')
class Comment(MongoEmbedded):
    when      = MongoProp.DateTimeField(default=datetime.now, required=True, editable=False)

    author    = MongoProp.ReferenceField(Contact)
    content   = MongoProp.StringField(verbose_name="Comment", required=True)

    class RDF: # (Reactor.rdf.Descriptor):
        namespace = ''

        def create(self, narrow):
            resp = Reactor.rdf.BNode() # a GUID is generated

            return resp

        def retrieve(self, narrow):
            resp = Reactor.rdf.URIRef("http://example.org/people/Bob")

            return resp

        def describe(self, entry, subject):
            yield subject, self.prefix('rdf','type'), self.prefix('foaf','Person')

            yield subject, self.prefix('foaf','name'), self.literal(entry.author.fullname)
            yield subject, self.prefix('foaf','age'), self.literal(24)
            yield subject, self.prefix('foaf','email'), self.literal(entry.author.fullname)

            #yield subject, self.prefix('foaf','knows'), self.literal(entry[])

#*******************************************************************************

@Reactor.mongo.register_embed('reaction')
class Reaction(MongoEmbedded):
    when      = MongoProp.DateTimeField(default=datetime.now, required=True, editable=False)

    author    = MongoProp.ReferenceField(Contact)
    action    = MongoProp.StringField(verbose_name="Action", required=True)

################################################################################

@Reactor.mongo.register_embed('person')
class Person(MongoDocument):
    address  = MongoProp.StringField(max_length=255, required=True, primary_key=True)
    fullname = MongoProp.StringField(max_length=255, required=True)

#*******************************************************************************

class PostMixin(object):
    owner_id  = MongoProp.IntField(required=True)
    source    = MongoProp.StringField(max_length=255, required=True)
    ontology  = MongoProp.StringField(max_length=255, required=True)
    narrow    = MongoProp.StringField(max_length=255, required=True, primary_key=True)
    partition = MongoProp.StringField(max_length=255, required=False, default='')

    when = MongoProp.DateTimeField(
        default=datetime.now, required=True, editable=False,
    )
    author    = MongoProp.ReferenceField(Contact)

    story     = MongoProp.StringField(required=False)
    cover     = MongoProp.StringField(required=False)

    #***************************************************************************

    class Admin(Reactor.mongo.SchemaAdmin):
        PERMISSIONs = ('read','delete')

        search_fields = ('owner_id', 'source', 'ontology')
        list_fields = ('owner_id', 'source', 'ontology', 'narrow', 'partition', 'author', 'when', 'story')

    #***************************************************************************

    @property
    def icon(self):
        resp = None

        if self.source=='facebook':
            resp = {
                'article': 'document',

                'link':    'link',
                'picture': 'image',
                'video':   'video',
            }.get(self.ontology, None)

        if resp is None:
            resp = 'pencil'

        return resp

    @classmethod
    def from_fb_entry(cls, user_id, entry, **payload):
        nrw = dict(
            owner_id = user_id,
            source   = 'facebook',
            ontology = entry['type'],
            narrow   = entry['id'],
        )

        if 'created_time' in entry:
            payload['when']   = dt_parser(entry['created_time'])
        else:
            payload['when']   = timezone.now()

        payload['author'] = upsert(Contact, dict(
            address  = '%(id)s@facebook.com' % entry['from'],
        ),dict(
            fullname = entry['from']['name'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'picture': 'cover',
            'story':   'story',
        }.iteritems():
            payload[trg] = entry.get(src, '')

        return nrw,payload

    @classmethod
    def from_tw_entry(cls, user_id, entry, **payload):
        nrw = dict(
            owner_id = user_id,
            source   = 'twitter',
            ontology = 'tweet',
            narrow   = entry.id_str,
        )

        payload['when']   = dt_parser(entry.created_at)

        payload['author'] = upsert(Contact, dict(
            address  = '%s@twitter.com' % entry.user.screen_name,
        ),dict(
            fullname = entry.user.name,

            intro    = entry.user.description,
            bio      = entry.user.description,

            mugshot  = entry.user.profile_image_url,

            #location = entry.user.location,
            phone    = '',
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'picture': 'cover',
            'story':   'story',
        }.iteritems():
            payload[trg] = getattr(entry, src, '')

        return nrw,payload

################################################################################

@Reactor.mongo.register_schema('mail_message')
class Mail_Message(MongoDocument, PostMixin):
    ns_alias = 'mail'
    ns_icon  = 'envelope'
    ns_types = []

    account    = MongoProp.StringField(max_length=255, required=False)

    field_from = property(lambda self: self.author)
    field_to   = MongoProp.ReferenceField(Contact)

    #field_cc   = MongoProp.ListField(MongoProp.ReferenceField(Contact))

    subject    = property(lambda self: self.story)
    content    = MongoProp.StringField(max_length=255)

    @classmethod
    def from_google(cls, user_id, entry, **payload):
        from base64 import urlsafe_b64decode
        from email import message_from_string

        msg = message_from_string(urlsafe_b64decode(entry['raw'].encode('ASCII')))

        nrw = dict(
            owner_id   = user_id,
            source     = 'google',
            ontology   = 'message',
            narrow     = entry['id'],
            partition  = entry['threadId'],
        ),dict(
            when       = datetime.fromtimestamp(entry['internalDate']),

            author     = msg['from'],
            field_to   = msg['to'],

            field_cc   = msg.get('cc',''),

            story      = msg['subject'],
            content    = msg.as_string(),
        )

        payload['field_from'] = upsert(Contact, dict(
            address  = msg['from'],
        ),dict(
            fullname = msg['from'],
        ))
        payload['field_to'] = upsert(Contact, dict(
            address  = msg['to'],
        ),dict(
            fullname = msg['to'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        return nrw,payload

    @classmethod
    def from_mailbox(cls, user_id, entry, **payload):
        nrw = dict(
            owner_id = user_id,
            source   = 'mail',
            ontology = 'message',
            narrow   = entry['id'],
        ),dict(
            when     = dt_parser(entry['created_time']),

            story    = entry['name'],
            cover    = entry['cover']['source'],

            summary  = entry['description'],

            start_at = dt_parser(entry['start_time']),
            ends_at  = dt_parser(entry['end_time']),
        )

        payload['author'] = upsert(Contact, dict(
            address  = '%(id)s@facebook.com' % entry['from'],
        ),dict(
            fullname = entry['from']['name'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'picture': 'cover',
        }.iteritems():
            payload[trg] = entry.get(src, '')

        for src,trg in {
            'start_time': 'start_at',
            'end_time':   'ends_at',
        }.iteritems():
            try:
                payload[trg] = dt_parser(entry.get(src, ''))
            except:
                payload[trg] = None

        return nrw,payload

#*******************************************************************************

@Reactor.mongo.register_schema('mail_attachment')
class Attachment(MongoDocument):
    message    = MongoProp.ReferenceField(Mail_Message)
    when       = MongoProp.DateTimeField(default=datetime.now, required=True, editable=False)

    filename   = MongoProp.StringField(max_length=255)
    mimetype   = MongoProp.StringField(max_length=255)

    content    = MongoProp.FileField(required=False)

################################################################################

class Post(MongoDocument, PostMixin):
    ns_alias = 'post'
    ns_icon  = 'print'
    ns_types = []

    @property
    def icon(self):
        resp = None

        if self.source=='facebook':
            resp = {
                'article': 'document',

                'link':    'link',
                'picture': 'image',
                'video':   'video',
            }.get(self.ontology, None)

        if resp is None:
            resp = 'pencil'

        return resp

#*******************************************************************************

@Reactor.mongo.register_schema('web_article')
class ArticlePost(MongoDocument, PostMixin):
    ns_alias = 'article'
    ns_icon  = 'file-text-o'
    ns_types = []

    title      = MongoProp.StringField(max_length=255)
    content    = MongoProp.StringField(required=True)

    # hashtags   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('CustomTag'), required=False)
    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.content)

    @classmethod
    def from_facebook(cls, user_id, item):
        return cls.from_fb_entry(user_id, item,
            content = item['content'],
        )

#*******************************************************************************

@Reactor.mongo.register_schema('web_status')
class StatusPost(MongoDocument, PostMixin):
    ns_alias = 'status'
    ns_icon  = 'comment'
    ns_types = ['tweet']

    message    = MongoProp.StringField()
    #location   = MongoProp.EmbeddedDocumentField('Location')

    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.message)

    @classmethod
    def from_facebook(cls, user_id, item):
        return cls.from_fb_entry(user_id, item,
            message = item.get('message',''),
        )

    @classmethod
    def from_twitter(cls, user_id, item):
        return cls.from_tw_entry(user_id, item,
            message = item.text,
        )

#*******************************************************************************

@Reactor.mongo.register_schema('web_link')
class LinkPost(MongoDocument, PostMixin):
    ns_alias = 'link'
    ns_icon  = 'link'
    ns_types = ['bookmark','web-article']

    summary    = MongoProp.StringField(required=False)

    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.summary)

    @classmethod
    def from_facebook(cls, user_id, item):
        return cls.from_fb_entry(user_id, item,
            summary = item.get('description',''),
        )

    @classmethod
    def from_pocket(cls, user_id, entry, **payload):
        user = Identity.objects.get(pk=user_id)

        nrw = dict(
            owner_id = user_id,
            source   = 'pocket',
            ontology = 'bookmark',
            narrow   = entry['item_id'],
        )

        if entry.get('is_article', False):
            nrw['ontology'] = 'web-article'

        payload['when']   = timezone.now()

        payload['author'] = upsert(Contact, dict(
            address  = user.email,
        ),dict(
            fullname = user.full_name,
        ))

        payload['title']   = entry['given_title']

        for dest,src in [
            ('summary','excerpt'),
        ]:
            if src in entry:
                payload[dest] = entry[src]

        #payload['target']  = entry['given_url']

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'picture':  'cover',
            'story':    'story',
            #'favorite': 'is_fav',
            #'status':   'is_read',
        }.iteritems():
            payload[trg] = entry.get(src, '')

        if 'images' in entry:
            pics = entry['images'].values()

            if len(pics):
                payload['cover'] = pics[0]['src']

        return nrw,payload

#*******************************************************************************

@Reactor.mongo.register_schema('web_image')
class ImagePost(MongoDocument, PostMixin):
    ns_alias = 'image'
    ns_icon  = 'camera'
    ns_types = []

    summary    = MongoProp.StringField()
    media_url  = MongoProp.StringField()

    #location   = MongoProp.EmbeddedDocumentField('Location')

    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.summary)

    @classmethod
    def from_facebook(cls, user_id, item):
        return cls.from_fb_entry(user_id, item,
            summary = item.get('description',''),
            media_url = item.get('full_picture',''),
        )

    @classmethod
    def from_instagram(cls, user_id, item):
        nrw = dict(
            owner_id = user_id,
            source   = 'instagram',
            ontology = entry['type'],
            narrow   = entry['id'],
        )

        payload = dict(
            summary = item['caption']['text'],
            media_url = item['images']['standard_resolution']['url'],
        )

        if 'created_time' in entry:
            payload['when']   = dt_parser(entry['created_time'])
        else:
            payload['when']   = timezone.now()

        payload['author'] = upsert(Contact, dict(
            address  = '%(id)s@instagram.com' % entry['user']['username'],
        ),dict(
            fullname = entry['user']['full_name'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        return nrw,payload

#*******************************************************************************

@Reactor.mongo.register_schema('web_video')
class VideoPost(MongoDocument, PostMixin):
    ns_alias = 'video'
    ns_icon  = 'video-camera'
    ns_types = []

    summary    = MongoProp.StringField()
    media_url  = MongoProp.StringField()

    #location   = MongoProp.EmbeddedDocumentField('Location')

    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.summary)

    @classmethod
    def from_facebook(cls, user_id, item):
        return cls.from_fb_entry(user_id, item,
            summary = item.get('description',''),
            media_url = item.get('source',''),
        )

#*******************************************************************************

@Reactor.mongo.register_schema('web_story')
class StoryPost(MongoDocument, PostMixin):
    ns_alias = 'story'
    ns_icon  = 'rss'
    ns_types = []

    title      = MongoProp.StringField()
    content    = MongoProp.StringField()
    target     = MongoProp.StringField()

    #location   = MongoProp.EmbeddedDocumentField('Location')

    # reactions  = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Reaction'), required=False)
    # comments   = MongoProp.ListField(MongoProp.EmbeddedDocumentField('Comment'), required=False)

    text       = property(lambda self: self.story.replace('\n',' '))

    start_at   = property(lambda self: self.when)
    is_all_day = property(lambda self: True)

    permalink  = property(lambda self: self.target)

    @classmethod
    def from_feedparser(cls, user_id, item):
        nrw,payload = dict(
            owner_id = user_id,
            source   = 'rss',
            ontology = 'news',
            narrow   = item.id,
        ),dict(
            title   = item.title,
            content = item.description,
            target  = item.link,
        )

        if hasattr(item, 'author'):
            payload['author'] = upsert(Contact, dict(
                address  = item.author,
                fullname = item.author,
            ),dict(
            ))
        else:
            payload['author'] = upsert(Contact, dict(
                address  = '%s@%s' % ('contact', urlparse(item.link).hostname),
                fullname = '%s@%s' % ('contact', urlparse(item.link).hostname),
            ),dict(
            ))

        payload['cover'] = '#' # entry.cover
        payload['story'] = item.title

        if hasattr(item, 'published'):
            payload['when']  = dt_parser(item.published)
        else:
            payload['when']  = timezone.now()

        #payload['reactions'] = []
        #payload['comments']  = []

        return nrw,payload

    @classmethod
    def from_facebook(cls, user_id, item, me):
        nrw,payload = dict(
            owner_id = user_id,
            source   = 'facebook',
            ontology = 'news',
            narrow   = item.id,
        ),dict(
            when    = dt_parser(item.date),

            title   = item.title,
            content = item.text,
            target  = item.url,
        )

        payload['author'] = upsert(Contact, dict(
            address  = '%(id)s@facebook.com' % me,
        ),dict(
            fullname = me['name'],

            intro    = me.get('about', ''),
            bio      = me.get('bio', ''),

            mugshot  = me.get('cover', {}).get('source', '#'),

            #location = me.get('location', {}),
            phone    = me.get('phone', ''),
        ))

        payload['cover'] = '#' # entry.cover
        payload['story'] = item.text

        #payload['reactions'] = []
        #payload['comments']  = []

        return nrw,payload

        if post.comments > 0:
            print '%i comments' % post.comments 
            print [(r.text, r.author) for r in fb.search(post.id, type=COMMENTS)]

        if post.likes > 0:
            print '%i likes' % post.likes 
            print [r.author for r in fb.search(post.id, type=LIKES)]

#*******************************************************************************

class EventPost(MongoDocument, PostMixin):
    ns_alias = 'event'
    ns_icon  = 'calendar'
    ns_types = []

    summary   = MongoProp.StringField()
    place     = MongoProp.DictField()

    start_at  = MongoProp.DateTimeField(required=True)
    ends_at   = MongoProp.DateTimeField(required=False, null=True, default=None)

    text       = property(lambda self: self.story.replace('\n',' '))

    @property
    def permalink(self):
        if self.source=='facebook':
            if self.ontology=='event':
                return 'https://www.facebook.com/events/%s' % self.narrow

        return '/calendar/%s' % self.id

    @classmethod
    def from_facebook(cls, user_id, entry, **payload):
        nrw,payload = dict(
            owner_id  = user_id,
            source    = 'facebook',
            ontology  = 'event',
            narrow    = entry['id'],
        ),dict(
            when      = dt_parser(entry['start_time']),

            story     = entry['name'],
            cover     = entry.get('cover',{}).get('source','#'),

            summary   = entry.get('description',''),
            place     = entry['place'],
        )

        payload['author'] = upsert(Contact, dict(
            address  = '%(id)s@facebook.com' % entry['owner'],
        ),dict(
            fullname = entry['owner']['name'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'start_time': 'start_at',
            'end_time':   'ends_at',
        }.iteritems():
            if src in entry:
                try:
                    payload[trg] = dt_parser(entry[src])
                except:
                    payload[trg] = entry[src]

        return nrw,payload

    @classmethod
    def from_google(cls, user_id, entry, **payload):
        nrw,payload = dict(
            owner_id  = user_id,
            source    = 'google',
            ontology  = 'event',
            narrow    = entry['id'],
            partition = entry['calendar']['id'],
        ),dict(
            when      = dt_parser(entry['created']),

            story     = entry['name'],
            cover     = entry['cover']['source'],

            summary   = entry['description'],
            place     = {
                'raw': entry['location'],
            },
        )

        payload['author'] = upsert(Contact, dict(
            address  = entry['creator']['email'],
        ),dict(
            fullname = entry['creator']['displayName'],
        ))

        #payload['reactions'] = []
        #payload['comments']  = []

        for src,trg in {
            'start': 'start_at',
            'end':   'ends_at',
        }.iteritems():
            try:
                payload[trg] = dt_parser(entry[src]['dateTime'])
            except:
                payload[trg] = entry[src]['dateTime']

        return nrw,payload

################################################################################

CONTACT_TYPEs = (Contact,)

MAIL_TYPEs = (Mail_Message,)

#*******************************************************************************

CALENDAR_TYPEs = (StoryPost, EventPost)

TIMELINE_TYPEs = (Post, ArticlePost, StatusPost, LinkPost, ImagePost, VideoPost, StoryPost, EventPost)

#*******************************************************************************

POST_TYPEs = (Post, ArticlePost, StatusPost, LinkPost, ImagePost, VideoPost, StoryPost, EventPost)

ACTIVITY_TYPEs = (StatusPost, ImagePost, VideoPost, EventPost)

#*******************************************************************************

ALL_TYPEs = set([x for lst in (
    CONTACT_TYPEs, MAIL_TYPEs,
    CALENDAR_TYPEs, TIMELINE_TYPEs,
    ACTIVITY_TYPEs, POST_TYPEs,
) for x in lst])

################################################################################

populate_func_document = lambda item: (item.id,item)
populate_func_contact  = lambda item: (item.author.address,item.author)

populate_sort_document = True, (lambda item: item['when'])
populate_sort_contact  = False, (lambda item: item['fullname'])

#*******************************************************************************

EXCLUDE = {
    Contact: ['owner_id', 'source', 'ontology__in'],
}

def populate_schema(ontology, **lookup):
    if not issubclass(ontology, Post):
        if ontology in EXCLUDE:
            for field in EXCLUDE[ontology]:
                if field in lookup:
                    del lookup[field]

        yield ontology.objects(**lookup)

    if issubclass(ontology, Post):
        lookup['ontology__in'] = ontology.ns_types+[ontology.ns_alias]

        if ontology in EXCLUDE:
            for field in EXCLUDE[ontology]:
                if field in lookup:
                    del lookup[field]

        yield Post.objects(**lookup)

#*******************************************************************************

def populate_schemas(extractor, classifier, *ontologies, **lookup):
    resp = {}

    for onto in ontologies:
        for query in populate_schema(onto, **lookup):
            for item in query:
                key,doc = extractor(item)

                resp[key] = doc

    rev,algo = classifier

    return sorted(resp.values(), key=algo, reverse=rev)

#*******************************************************************************

from django.core.cache import caches

def enumerate_schemas(*ontologies, **lookup):
    vault = caches['default']

    if len(ontologies)==0:
        ontologies = ALL_TYPEs

    resp,icon = {},{}

    for onto in ontologies:
        count = vault.get('enumerate_schema_%s' % onto.ns_alias)

        if count is None:
            count = 0

            for query in populate_schema(onto, **lookup):
                count += len(query)

            vault.set('enumerate_schema_%s' % onto.ns_alias, count, 60 * 5)

        resp[onto.ns_alias] = count

        icon[onto.ns_alias] = onto.ns_icon

    lst = [
        (k.capitalize(), icon[k], str(v))
        for k,v in resp.iteritems()
    ]

    return sorted(lst, key=lambda x: x[0], reverse=False)

