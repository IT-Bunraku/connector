import os, re

from datetime import datetime, timedelta

from django import template
from django.utils import timezone

from social.backends.oauth import OAuthAuth

#*******************************************************************************

register = template.Library()

name_re = re.compile(r'([^O])Auth')

################################################################################

def humanize_date_difference(now, otherdate=None, offset=None):
    if otherdate:
        dt = otherdate - now
        offset = dt.seconds + (dt.days * 60*60*24)
    if offset:
        delta_s = offset % 60
        offset /= 60
        delta_m = offset % 60
        offset /= 60
        delta_h = offset % 24
        offset /= 24
        delta_d = offset
    else:
        raise ValueError("Must supply otherdate or offset (from now)")

    if delta_d > 1:
        if delta_d > 6:
            date = now + timedelta(days=-delta_d, hours=-delta_h, minutes=-delta_m)
            return date.strftime('%A, %Y %B %m, %H:%I')
        else:
            wday = now + timedelta(days=-delta_d)
            return wday.strftime('%A')

    if delta_d == 1:
        return "Yesterday"

    if delta_h > 0:
        return "%dh%dm ago" % (delta_h, delta_m)

    if delta_m > 0:
        return "%dm%ds ago" % (delta_m, delta_s)
    else:
        return "%ds ago" % delta_s

#*******************************************************************************

def fa_icon(name):
    mapping = {
        'email':                'envelope',
        'mail':                 'envelope',
        'username':             'user',

        'google-openidconnect': 'google',
        'live':                 'windows',
        'facebook-app':         'facebook',
        'salesforce-oauth2':    'leaf',

        'vimeo':                'vimeo-square',
        'stackoverflow':        'stack-overflow',
    }

    mapping.update({
        'coinbase':             'bitcoin',
        'twilio':               'phone',
        'uber':                 'car',
        'upwork':               'briefcase',

        'podio':                'podcast',
        'qiita':                'quora', # qq
        'wunderlist':           'bookmark',
        'zotero':               'book',
    })

    for key in (
        'google','yahoo',
        'vk','facebook','twitter',
        'linkedin',
    ):
        mapping['%s-oauth' % key]  = key
        mapping['%s-oauth2' % key] = key

    mapping.update({
        'rss':                  'rss',
        'wp':                   'wordpress',
        'dyno':                 'server',
        'backend':              'database',
    })

    return mapping.get(name, name)

################################################################################

@register.simple_tag(takes_context=True)
def media(context, path):
    return '/media/'+path

#*******************************************************************************

@register.filter
def since(target):
    return humanize_date_difference(timezone.now(), target)

#*******************************************************************************

@register.filter
def js_date(target):
    resp = [target.year, target.month, target.day]

    if hasattr(target, 'hour'):
        resp += [target.hour, target.minute]

    return "new Date(%s)" % ', '.join([str(x) for x in resp])

################################################################################

def filter_fqdns(listing, narrow, callback):
    resp = [] ; curr = None

    for entry in listing:
        if entry.alias==narrow:
            curr = entry

    special = {
        'connector': 'connect',
        'console':   'console',
        'organizer': 'mail',
        'devops':    'cloud',
        'semantics': 'graph',
        'opendata':  'data',
    }

    if curr is not None:
        for fqdn in listing:
            if callback(fqdn.app_name, curr.app_name):
                resp.append(fqdn)
            elif fqdn.app_name in special:
                if callback(special[fqdn.app_name], curr.alias):
                    resp.append(fqdn)

    else:
        for fqdn in listing:
            if fqdn.app_name in special:
                resp.append(fqdn)

    return resp

@register.filter
def related_fqdns(listing, narrow):
    return filter_fqdns(listing, narrow, lambda x,y: x==y)

@register.filter
def other_fqdns(listing, narrow):
    return filter_fqdns(listing, narrow, lambda x,y: x!=y)

################################################################################

@register.filter
def attribute(target, field):
    resp = None

    if hasattr(target, field):
        resp = getattr(target, field)
    elif field in target:
        resp = target[field]

    if resp is not None:
        if field.lower().strip() in ('xml:lang','lang','language'):
            resp = {
                'ar': 'ma',
                'en': 'us',
                'ja': 'jp',
            }.get(resp, resp)

    return resp

@register.filter
def pretty_query(statement):
    for src,dest in [
        ('<','&lt;'),
        ('>','&gt;'),
        ('\n\n','\n'),
        ('\n','<br/>'),
    ]:
        while src in statement:
            statement = statement.replace(src, dest)

    return statement

@register.filter
def nlp_style(entity, roles):
    resp = {
        'color': {
            'CC':   'orange',
            'DT':   'goldenrod',
            'IN':   'orange',

            'VBD':  'red',
            'VBN':  'red',

            'NN':   'green',
            'NP':   'green',

            'NNP':  'blue',
            'NNPS': 'blue',
        }.get(str(roles[-1]).upper(), 'black'),
    }

    return "color: %(color)s;" % resp

################################################################################

@register.filter
def backend_name(backend):
    name = backend.__class__.__name__
    name = name.replace('OAuth', ' OAuth')
    name = name.replace('OpenId', ' OpenId')
    name = name.replace('Sandbox', '')
    name = name_re.sub(r'\1 Auth', name)
    return name

@register.filter
def backend_class(backend):
    return backend.name.replace('-', ' ')

#*******************************************************************************

@register.filter
def backend_icon(backend):
    return fa_icon(backend.name)

@register.filter
def icon_name(name):
    return fa_icon(name)

#*******************************************************************************

@register.simple_tag(takes_context=True)
def social_begin(context, backend):
    return '//apis.%s/social/login/%s' % (os.environ['GHOST_DOMAIN'], backend)

@register.simple_tag(takes_context=True)
def social_finish(context, backend):
    return '//apis.%s/social/disconnect/%s' % (os.environ['GHOST_DOMAIN'], backend)

#*******************************************************************************

@register.filter
def login_backends(backends):
    backends = [
        backend for name, backend in backends.items()
        if backend.name in (
            'google-oauth2','yahoo-oauth2','salesforce-oauth2',
            'facebook','twitter','linkedin-oauth2',
            'github','slack',
        )
    ]
    backends.sort(key=lambda b: b.name)
    return backends


@register.filter
def social_backends(backends):
    backends = [backend for name, backend in backends.items()
                    if name not in ['username', 'email']]
    backends.sort(key=lambda b: b.name)
    return backends


@register.filter
def legacy_backends(backends):
    backends = [backend for name, backend in backends.items()
                    if name in ['username', 'email']]
    backends.sort(key=lambda b: b.name)
    return backends


@register.filter
def oauth_backends(backends):
    backends = [backend for name, backend in backends.items()
                    if issubclass(backend, OAuthAuth)]
    backends.sort(key=lambda b: b.name)
    return backends

#*******************************************************************************

@register.simple_tag(takes_context=True)
def associated(context, backend):
    user = context.get('user')
    context['association'] = None
    if user and user.is_authenticated():
        try:
            context['association'] = user.social_auth.filter(
                provider=backend.name
            )[0]
        except IndexError:
            pass
    return ''

