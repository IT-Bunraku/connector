from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

################################################################################

OPENGRAPH_VERSION = '2.5'

OPENGRAPH_IDEN_FIELDs = (
    'id','is_verified',
    'name','link','location',
    'about','bio','cover',
    'birthday','context',
)
OPENGRAPH_PAGE_FIELDs = OPENGRAPH_IDEN_FIELDs+(
    #'age',
)
OPENGRAPH_USER_FIELDs = OPENGRAPH_IDEN_FIELDs+(
    'email',
    'first_name','middle_name','last_name',
    'gender','interested_in','meeting_for','relationship_status',
    'locale','hometown',
    'education','quotes','inspirational_people',
    'political','religion','test_group',
    'currency','payment_pricepoints',
    'sports','favorite_athletes','favorite_teams',
)

OPENGRAPH_POST_FIELDs = (
    'application','actions','created_time','child_attachments','caption','event','description','from','full_picture','icon','height','id','coordinates','instagram_eligibility','comments_mirroring_domain','link','message','object_id','parent_id','permalink_url','name','picture','place','privacy','properties','shares','source','status_type','story','story_tags','subscribed','type','updated_time','via','width','attachments','sharedposts','to','message_tags',
    'likes','comments', # 'reactions',
)
OPENGRAPH_EVENT_FIELDs = (
    'owner','name','type','place',
    'timezone','start_time','end_time','updated_time',
    'description','category',
    'picture','cover','feed','posts','photos','videos',
    'rsvp_status','attending_count','declined_count','interested_count','maybe_count','noreply_count'
    'interested','is_canceled','is_page_owned','roles',
    'maybe','noreply','attending','declined',
)

import facebook

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_facebook(user_id, **response):
    user = Identity.objects.get(pk=user_id)

    graph = facebook.GraphAPI(access_token=response['access_token'], version=OPENGRAPH_VERSION)

    this = graph.get_object('/me?fields='+','.join(OPENGRAPH_USER_FIELDs))

    social_facebook_posts.delay(user_id, response['access_token'], this['id'], True)

    for entry in graph.get_object('/me/friends')['data']:
        social_facebook_posts.delay(user_id, response['access_token'], entry['id'], True)

        #for item in graph.get_object('/%(id)s/likes' % entry)['data']:
        #    social_facebook_posts.delay(user_id, response['access_token'], item['id'], False)
    
    for entry in graph.get_object('/me/likes')['data']:
        social_facebook_posts.delay(user_id, response['access_token'], entry['id'], False)

    for entry in graph.get_object('/me/accounts')['data']:
        social_facebook_posts.delay(user_id, entry['access_token'], entry['id'], False)

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_facebook_posts(user_id, api_token, target_id='me', is_user=True):
    if type(api_token) is dict:
        if 'oauth_token_secret' in api_token:
            api_token = api_token['oauth_token_secret']

    this = None ; user = Identity.objects.get(pk=user_id)

    graph = facebook.GraphAPI(access_token=api_token, version=OPENGRAPH_VERSION)

    if is_user:
        this = graph.get_object('/%s?fields=%s' % (target_id, ','.join(OPENGRAPH_USER_FIELDs)))
    else:
        this = graph.get_object('/%s?fields=%s' % (target_id, ','.join(OPENGRAPH_PAGE_FIELDs)))

    resp = graph.get_object('/%s/posts?fields=%s' % (target_id, ','.join(OPENGRAPH_POST_FIELDs)))

    while type(resp) in [dict]:
        if 'data' in resp:
            for entry in resp['data']:
                onto = {
                    'link':    LinkPost,
                    'picture': ImagePost,
                    'video':   VideoPost,

                    'article': ArticlePost,
                }.get(entry['type'], StatusPost)

                upsert(onto, *onto.from_facebook(user_id, entry))

        hist = resp
        resp = None

        if 'paging' in hist:
        #for key in ['previous','next']:
            for key in ['next']:
                if key in hist['paging']:
                    req = requests.get(hist['paging'][key])

                    resp = req.json()

    resp = graph.get_object('/%s/events?fields=%s' % (target_id, ','.join(OPENGRAPH_EVENT_FIELDs)))

    while type(resp) in [dict]:
        for entry in resp['data']:
            upsert(EventPost, *EventPost.from_facebook(user_id, entry))

        hist = resp
        resp = None

        if 'paging' in hist:
            #for key in ['previous','next']:
            for key in ['next']:
                if key in hist['paging']:
                    req = requests.get(hist['paging'][key])

                    resp = req.json()

    from pattern.web import Facebook, NEWS, COMMENTS, LIKES

    fb = Facebook(license=api_token)

    for entry in fb.search(target_id, type=NEWS, count=500):
        upsert(StoryPost, *StoryPost.from_facebook(user_id, entry, this))

################################################################################

import twitter

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_twitter(user_id, **response):
    user = Identity.objects.get(pk=user_id)

    api = twitter.Api(
        consumer_key=settings.SOCIAL_AUTH_TWITTER_KEY,
        consumer_secret=settings.SOCIAL_AUTH_TWITTER_SECRET,
        access_token_key=settings.SOCIAL_AUTH_TWITTER_ACCESS_KEY,
        access_token_secret=settings.SOCIAL_AUTH_TWITTER_ACCESS_SECRET,
    )

    # api.(response['access_token'])

    for entry in api.GetFriends():
        social_facebook_posts.delay(user_id, response['access_token'], entry.id)

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_twitter_posts(user_id, api_token, tw_user):
    this = None ; user = Identity.objects.get(pk=user_id)

    api = twitter.Api(
        consumer_key=settings.SOCIAL_AUTH_TWITTER_KEY,
        consumer_secret=settings.SOCIAL_AUTH_TWITTER_SECRET,
        access_token_key=settings.SOCIAL_AUTH_TWITTER_ACCESS_KEY,
        access_token_secret=settings.SOCIAL_AUTH_TWITTER_ACCESS_SECRET,
    )

    # api.(response['access_token'])

    for entry in api.GetUserTimeline(user_id=tw_user):
        upsert(StatusPost, *StatusPost.from_twitter(user_id, entry))

################################################################################

from instagram.client import InstagramAPI

#*******************************************************************************

class Submitter(object):
    def __init__(self, user_id, access_token):
        user = Identity.objects.get(pk=user_id)

#*******************************************************************************

class InstagramSync(Submitter):
    def initialize(self):
        pass

    def request(self, verb, path, payload={}):
        pass

    def process(self, response):
        pass

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_instagram(user_id, **response):
    social_instagram_user.delay(user_id, response['access_token'], 'self', 1)

#*******************************************************************************

def instagram_request(*args, **kwargs):
    def do_apply(callback, target, query='', access_token=None):
        req = requests.get('https://api.instagram.com/v1/%s?access_token=%s&%s' % (path, access_token, query))

        resp = req.json()

        if 'data' in resp:
            callback(resp['data'])

    return lambda handler: do_apply(handler, *args, **kwargs)

@Reactor.rq.register_task(queue='social')
def social_instagram_user(owner_id, access_token, alias, depth):
    @instagram_request('users/%s' % alias, '', access_token)
    def process_profile(entry):
        vcard = upsert(Contact, dict(
            address  = '%(id)s@instagram.com' % entry['username'],
        ),dict(
            fullname = entry['full_name'],
            mugshot  = entry['profile_picture'],
        ))

        social_instagram_media.delay(owner_id, access_token, entry['username'])
 
    if alias in ['self']:
        @instagram_request('users/%s/follows' % alias, '', access_token)
        def process_follows(data):
            for entry in data:
                social_instagram_user.delay(owner_id, access_token, entry['username'], depth+1)

        @instagram_request('users/%s/followed-by' % alias, '', access_token)
        def process_followed_by(data):
            for entry in data:
                social_instagram_user.delay(owner_id, access_token, entry['username'], depth+1)

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_instagram_media(owner_id, access_token, alias):
    @instagram_request('users/%s/media/recent' % alias, '', access_token)
    def process_recent(data):
        for entry in data:
            onto = {
                'video':   VideoPost,
            }.get(entry['type'], ImagePost)

            media = upsert(onto, *onto.from_instagram(owner_id, entry))

