from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

from . import SocialAPI

from . import Corporate
from . import Productivity

################################################################################

@Reactor.rq.register_task(queue='default')
def refresh_identity(user_id, *networks, **mapping):
    user = Identity.objects.get(pk=user_id)

    #if mapping.get('mail',False):
    for mailbox in user.mailboxes.all():
        Reactor.rq.delay_task('refresh_mailbox', mailbox.pk)

    #if mapping.get('feed',False):
    for feed in user.rss_feeds.all():
        Reactor.rq.delay_task('refresh_rssfeed', feed.pk)

    #if mapping.get('wordpress',False):
    #for site in user.wp_sites.all():
    #    Reactor.rq.delay_task('refresh_wordpress', site.pk)

    networks = [net for net in networks]

    for backend in user.social_auth.all():
        key = backend.provider

        for part in ('-oauth2', '-oauth'):
            if part in key:
                key = key.replace(part, '')

        if (key in networks) or len(networks)==0:
            cnt = backend.extra_data

            if type(cnt) in [str, unicode]:
                cnt = json.loads(cnt)

            if 'access_token' in cnt:
                if type(cnt['access_token']) is dict:
                    if 'oauth_token_secret' in cnt['access_token']:
                        cnt.update(cnt['access_token'])

                        cnt['access_token'] = cnt['access_token']['oauth_token_secret']

            if 'user_id' in cnt:
                del cnt['user_id']

            resp = Reactor.rq.delay_task('social_%s' % key, user.pk, **cnt)

            if resp is None:
                print "Could'nt refresh '%s' backend !" % key

################################################################################

@Reactor.rq.register_task(queue='default')
def refresh_social(user_id, *networks, **mapping):
    user = Identity.objects.get(pk=user_id)

    import uchikoma.core.apis

    apis = Reactor.social.api_manager(user)

    if len(networks):
        networks = apis.keys()

    for key in networks:
        if key in apis:
            for entry in apis.discover():
                print entry

