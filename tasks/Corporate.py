from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

import requests

################################################################################

def request_google(link, **params):
    req = requests.get(link, params=params)

    resp = req.json()

    return resp

#*******************************************************************************

def remap_google(link, **params):
    resp = request_google(link, **params)

    state = False

    for key in ('items', 'messages'):
        if key in resp:
            for entry in resp[key]:
                yield entry

            state = True

    if state:
        print resp

#*******************************************************************************

@Reactor.rq.register_task(queue='social')
def social_google(user_id, **response):
    params = {
        'access_token': response['access_token'],
    }

    user = Identity.objects.get(pk=user_id)

    args = (user_id,'google')

    for friend in remap_google('https://people.googleapis.com/v1/people/me/connections', **params):
        onto = Contact

        #upsert(onto, *onto.from_google(user_id, friend))

        downsert(Person, event, *args)

    for entry in remap_google('https://www.googleapis.com/gmail/v1/users/me/messages', **params):
        downsert(Mail_Message, request_google('https://www.googleapis.com/gmail/v1/users/me/messages/%(id)s' % entry, **params), *args)

        downsert(Mail_Thread, request_google('https://www.googleapis.com/gmail/v1/users/me/threads/%(threadId)s' % entry, **params), *args)

    for calendar in remap_google('https://www.googleapis.com/calendar/v3/users/me/calendarList', **params):
        for event in remap_google('https://www.googleapis.com/calendar/v3/calendars/%(id)s/events?' % calendar, **params):
            event['calendar'] = calendar

            downsert(EventPost, event, *args)

################################################################################


