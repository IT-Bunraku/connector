from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

################################################################################

def add(x, y):
    job = get_current_job()
    job.meta['handled_by'] = socket.gethostname()
    job.save()
    return x + y

################################################################################

class Helper(object):
    def trigger(self, method, *args, **kwargs):
        handler = getattr(self, method, None)

        if callable(handler):
            return handler(*args, **kwargs)
        else:
            return None

#*******************************************************************************

class SocialAPI(Helper):
    def __init__(self, user_id, api_token, method, *args, **kwargs):
        self._idn = Identity.objects.get(pk=user_id)
        self._key = api_token

        self._cnx = None

        self.trigger('prepare')

    @property
    def uplink(self):
        if self._cnx is None:
            self._cnx = self.trigger('connect')

        return self._cnx

#*******************************************************************************

class FacebookAPI(SocialAPI):
    def connect(self):
        pass

################################################################################

# api_delay('facebook', 'user')
# api_delay('facebook', 'page')
# api_delay('facebook', 'post')

################################################################################

def cleanup():
    for idn in Identity.objects.all():
        cleanup_identity.delay(idn.alias)

#j = schedule.enqueue_at(datetime(2020, 10, 10), cleanup)

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def cleanup_identity(narrow):
    qs = Identity.objects.filter(alias=narrow)

    if len(qs):
        idn = qs[0]

        for repo in idn.repos.all():
            if repo.alive():
                repo.state = 'sleepy'

        for dyno in idn.dynos.all():
            if dyno.alive():
                dyno.state = 'sleepy'

        for bkn in idn.backends.all():
            if bkn.alive():
                bkn.state = 'sleepy'

################################################################################

@Reactor.rq.register_task(queue='default')
def refresh_heroku(api_token):
    cnx = heroku.Application(token=api_token)

    for app in cnx.apps:
        resp = None

        for idn in Identity.objects.all():
            case = idn.match_heroku(app.name)

            if case:
                dyno, st = idn.dynos.get_or_create(
                    alias = case['alias'],
                )

                dyno.save()

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def refresh_github(api_token):
    pass

#*******************************************************************************

@Reactor.rq.register_task(queue='default')
def refresh_bitbucket(api_token):
    pass

