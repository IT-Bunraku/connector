from uchikoma.connector.utils import *

from uchikoma.connector.models  import *
from uchikoma.connector.schemas import *
from uchikoma.connector.graph   import *

#*******************************************************************************

from uchikoma import settings

################################################################################

import pocket

#*******************************************************************************

@Reactor.rq.register_task(queue='perso')
def social_pocket(user_id, **response):
    user = Identity.objects.get(pk=user_id)

    api = pocket.Pocket(settings.SOCIAL_AUTH_POCKET_KEY, response['access_token'])

    social_pocket_page.delay(user_id, 0, **response)

#*******************************************************************************

@Reactor.rq.register_task(queue='perso')
def social_pocket_page(user_id, page_index, **response):
    user = Identity.objects.get(pk=user_id)

    api = pocket.Pocket(settings.SOCIAL_AUTH_POCKET_KEY, response['access_token'])

    page_size = 100 ; lst = []

    resp,hds = api.get(state='all',sort='newest',detailType='complete', offset=page_index*page_size, count=page_size)

    lst = [x for x in resp['list'].values()]

    for entry in lst:
        upsert(LinkPost, *LinkPost.from_pocket(user_id, entry))

    if len(lst):
        social_pocket_page.delay(user_id, page_index+1, **response)

