# -*- coding: utf-8 -*-

from octopus.shortcuts import *

NUMBERs = [str(d) for d in range(0,10)]

class AdafruitSpider(CrawlSpider):
    name = "adafruit"
    allowed_domains = [
        "adafruit.com",
        "www.adafruit.com",
    ]

    start_urls = (
        'http://www.adafruit.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=('\/category\/(\\d+)$', )),    callback='parse_categ',   follow=True),
        Rule(LinkExtractor(allow=('\/product\/(\\d+)$', )),     callback='parse_product', follow=True),
        Rule(LinkExtractor(allow=('\/products\/(\\d+)$', )),    callback='parse_product', follow=True),
    )

    #######################################################################################################################

    def parse_categ(self, response):
        categ = RetailCategory()

        categ['link'] = response.url
        categ['site'] = self.name
        categ['uid']  = urlparse(response.url).path

        categ['title']      = cleanup(response.xpath('//div[contains(@class,"container")]//h1/text()'), sep=None, trim=True)

        #*****************************************************************************************************************

        categ['products']   = [
            {
                'uid': cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
                'title': cleanup(product.xpath('.//h1/a/text()'), sep=None, trim=True, inline=True),
                'url': cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
            }
            for product in response.xpath('//div[contains(@class, "row product-listing")]')
        ]

        #*****************************************************************************************************************

        yield categ

    #######################################################################################################################

    def parse_product(self, response):
        entry = RetailArticle()

        entry['link'] = response.url
        entry['site'] = self.name
        entry['uid']  = cleanup(response.xpath('//div[@class="product_id"]/text()'), sep=None, trim=True)

        entry['vendor'] = dict(
            name = cleanup(response.xpath('//div[@id="product"]//div[@itemprop="brand manufacturer"]/text()'), sep=None, trim=True),
        )

        entry['title']      = cleanup(response.xpath('//div[@id="prod-right-side"]/h1/text()'), sep=None, trim=True)
        entry['description'] = cleanup(response.xpath('//div[@id="description"]/p/text()'), sep='\n', trim=True)

        entry['stock']      = cleanup(response.xpath('//div[@id="prod-stock"]/div/text()'), sep=None, trim=True)

        entry['pricing']         = dict(
            per_unit=cleanup(response.xpath('//div[@id="prod-price"]/text()'), sep=None, trim=True),
        )

        entry['media'] = {
            'links': [
                self.start_urls[0] + link
                for link in response.xpath('//a[@data-fancybox-group="gallery"]/@href').extract()
            ],
        }

        #*****************************************************************************************************************

        yield entry
