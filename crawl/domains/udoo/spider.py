# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class UdooSpider(CrawlSpider):
    name = "udoo"
    allowed_domains = ["shop.udoo.org"]

    start_urls = (
        'http://shop.udoo.org/other/?___from_store=other&popup=no',
    )

    rules = (
        Rule(LinkExtractor(allow=('others\/(.*)\.html', )),        callback='parse_categ',   follow=True),
        Rule(LinkExtractor(allow=('others\/(.*)\/(.*)\.html', )),  callback='parse_product', follow=True),
    )

    def parse_categ(self, response):
        categ = RetailCategory()

        categ['site']       = self.name
        categ['link']       = response.url
        categ['uid']        = response.url.split('/collections/')[1]
        categ['title']      = cleanup(response.xpath('//div[@id="sub_collections"]/h2[1]/text()'), sep=None, trim=True)

        categ['products']   = [
            {
                'vendor': product.xpath('./@data-vendor').extract(),
                'uid':    product.xpath('./@data-id').extract(),
                'title':  product.xpath('./@data-title').extract(),
                'url':    product.xpath('./div[@class="col-md-3"]/a/@href').extract(),
            }
            for product in response.xpath('//div[contains(@class, "product")]')
        ]

        yield categ

    def parse_product(self, response):
        entry = RetailArticle()

        entry['site']       = self.name
        entry['link']       = response.url
        entry['uid']        = cleanup(response.xpath('//button[@class="add-to-cart-lg"]/@value'), sep=None, trim=True)

        entry['title']      = cleanup(response.xpath('//div[@id="title"]/h1//text()'), sep=None, trim=True)
        entry['description'] = cleanup(response.xpath('//div[@id="description"]/p/text()'), sep='\n', trim=True)

        entry['stock']      = cleanup(response.xpath('//strong[@class="stock-level"]//text()'), sep=None, trim=True)

        entry['pricing']         = dict(
            unit=cleanup(response.xpath('//div[@class="price"]/text()'), sep=None, trim=True),
        )

        entry['media'] = {
            'links': response.xpath('//*[@id="gallery"]/a/img/@src').extract(),
        }

        yield entry
