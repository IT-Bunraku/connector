from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

################################################################################

dag = DAG('sync_identity', default_args={
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2016, 12, 1),
    'email': ['amine@tayaa.me'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}, schedule_interval=timedelta(1))

#*******************************************************************************

from uchikoma.connector import tasks
from uchikoma.connector.models import Identity

for user in Identity.objects.all():
    tub = PythonOperator(python_callable=Reactor.rq.get_method('begin_identity'), provide_context=True,
        op_args=[user.pk],
        op_kwargs={},
    params={}, dag=dag, task_id="begin_identity")

    tuf = PythonOperator(python_callable=Reactor.rq.get_method('finish_identity'), provide_context=True,
        op_args=[user.pk],
        op_kwargs={},
    params={}, dag=dag, task_id="finish_identity")

    for mailbox in user.mailboxes.all():
        tm = PythonOperator(python_callable=Reactor.rq.get_method('refresh_mailbox'), provide_context=True,
            op_args=[mailbox.pk],
            op_kwargs={},
        params={}, dag=dag, task_id="refresh_mailbox_%s" % mailbox.pk)

        tub >> tm >> tuf
        
    #if mapping.get('feed',False):
    for feed in user.rss_feeds.all():
        tf = PythonOperator(python_callable=Reactor.rq.get_method('refresh_rssfeed'), provide_context=True,
            op_args=[feed.pk],
            op_kwargs={},
        params={}, dag=dag, task_id="refresh_rssfeed_%s" % feed.pk)

        tub >> tf >> tuf

    #if mapping.get('wordpress',False):
    for site in user.wp_sites.all():
        ts = PythonOperator(python_callable=Reactor.rq.get_method('refresh_wordpress'), provide_context=True,
            op_args=[site.pk],
            op_kwargs={},
        params={}, dag=dag, task_id="refresh_wordpress_%s" % site.pk)

        tub >> ts >> tuf

    networks = [net for net in networks]

    for backend in user.social_auth.all():
        key = backend.provider

        for part in ('-oauth2', '-oauth'):
            if part in key:
                key = key.replace(part, '')

        if (key in networks) or len(networks)==0:
            cnt = backend.extra_data

            if type(cnt) in [str, unicode]:
                cnt = json.loads(cnt)

            if 'access_token' in cnt:
                if type(cnt['access_token']) is dict:
                    if 'oauth_token_secret' in cnt['access_token']:
                        cnt.update(cnt['access_token'])

                        cnt['access_token'] = cnt['access_token']['oauth_token_secret']

            if 'user_id' in cnt:
                del cnt['user_id']

            hnd = Reactor.rq.get_method('social_%s' % key)

            if hnd is not None:
                tn = PythonOperator(python_callable=hnd, provide_context=True,
                    op_args=[user.pk],
                    op_kwargs=cnt,
                params={}, dag=dag, task_id="%s__social__%s" % (user.username, key))

                tub >> tn >> tuf
            else:
                print "Could'nt refresh '%s' backend !" % key

