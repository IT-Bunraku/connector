from datetime import datetime, timedelta

from airflow import DAG

from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.check_operator import CheckOperator, ValueCheckOperator, IntervalCheckOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators.subdag_operator import SubDagOperator

from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.docker_operator import DockerOperator

from airflow.operators.jdbc_operator import JdbcOperator
from airflow.operators.sqlite_operator import SqliteOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.mysql_operator import MySqlOperator
from airflow.operators.mssql_operator import MsSqlOperator
from airflow.operators.oracle_operator import OracleOperator

from airflow.operators.http_operator import SimpleHttpOperator
from airflow.operators.email_operator import EmailOperator
from airflow.operators.slack_operator import SlackAPIOperator, SlackAPIPostOperator

from airflow.operators.hive_operator import HiveOperator
from airflow.operators.pig_operator import PigOperator

from airflow.operators.generic_transfer import GenericTransfer

#*******************************************************************************

class FlowManager(object):
    def __init__(self):
        pass

    def register_op(self, *args, **kwargs):
        def do_apply(handler, dag, upstream=None):
            if upstream is not None:
                handler.set_upstream(upstream)

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    def register_func(self, *args, **kwargs):
        def do_apply(handler, dag, params={}, upstream=None):
            resp = PythonOperator(handler, provide_context=True,
                op_args=,
                op_kwargs=,
            params=params, dag=dag)

            resp = self.register_op(dag,
                upstream=upstream,
            )(resp)

            return resp

        return lambda hnd: do_apply(hnd, *args, **kwargs)

################################################################################

dag = DAG('sync_social', schedule_interval=timedelta(1), default_args={
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2016, 12, 1),
    'email': ['amine@tayaa.me'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    # 'queue': 'bash_queue',
    # 'pool': 'backfill',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
})

prm = {
    'user_id': 'int',
}

mgr = FlowManager(dag)

#*******************************************************************************

@mgr.process(dag, params=prm)
def sync_user(*args, **kwargs):
    print "Sync user (%s) { %s }" % (args, kwargs)

#*******************************************************************************

@mgr.process(dag, params=prm)
def store_user(ds, **kwargs):
    pprint(kwargs)
    print(ds)
    return 'Whatever you return gets printed in the logs'

################################################################################

@mgr.process(dag, params=prm, upstream=store_user)
def sync_facebook(*args, **kwargs):
    print "Sync facebook (%s) { %s }" % (args, kwargs)

################################################################################

@mgr.process(dag, params=prm, upstream=store_user)
def sync_twitter(*args, **kwargs):
    print "Sync twitter (%s) { %s }" % (args, kwargs)

################################################################################

@mgr.process(dag, params=prm, upstream=store_user)
def sync_instagram(*args, **kwargs):
    print "Sync instagram (%s) { %s }" % (args, kwargs)

